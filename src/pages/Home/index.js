import { useEffect, useState } from 'react';
import api from '../../services/api';
import { Link } from 'react-router-dom';
import './home.css';

function Home() {
    const [filmes, setFilmes] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        async function loadFilmes() {
            const response = await api.get("movie/now_playing");

            setFilmes(response.data.results);
            setLoading(false);
        }

        loadFilmes();
    }, [])

    if (loading) {
        return (
            <div className="loading">
                <h1>Carregando filmes...</h1>
            </div>
        );
    }

    return (
        <div className="container">
            <div className="lista-filmes">
                {
                    filmes.map((filme) => (
                        <article key={filme.id}>
                            <strong>{filme.title}</strong>
                            <img src={`https://image.tmdb.org/t/p/original${filme.poster_path}`} alt={filme.title} loading="lazy" />
                            <Link to={`/filme/${filme.id}`}>Acessar</Link>
                        </article>
                    ))
                }
            </div>
        </div>
    );
}

export default Home;