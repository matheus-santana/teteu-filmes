import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import api from '../../services/api';
import './filme.css';
import { toast } from 'react-toastify';

function Filme() {
    const { id } = useParams();
    const navigate = useNavigate();

    const [filme, setFilme] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        async function getFilme() {
            await api.get(`movie/${id}`)
                .then((response) => {
                setFilme(response.data);
                setLoading(false);
            }).catch(() => {
                navigate("/", { replace: true });
                return;
            });
        }

        getFilme();

        return () => { };
    }, [navigate, id]);

    function salvarFilme() {
        const minhaLista = localStorage.getItem('prime-flix');
        let filmesSalvos = JSON.parse(minhaLista) || [];

        const hasFilme = filmesSalvos.some((filmeSalvo) => filmeSalvo.id === filme.id);

        if (hasFilme) {
            toast.warn("Você já possui esse filme salvo.");
            return;
        }

        filmesSalvos.push(filme);
        localStorage.setItem('prime-flix', JSON.stringify(filmesSalvos));
        toast.success("Filme salvo com sucesso!");
    }

    if (loading) {
        return (
            <div className="filme-info">
                <h1>Carregando detalhes...</h1>
            </div>
        );
    }

    return (
        <div className="filme-info">
            <h1>{filme.title}</h1>
            <img src={`https://image.tmdb.org/t/p/original${filme.backdrop_path}`} alt={filme.title} loading="lazy" />
            <h3>Sinopse</h3>
            <span>{filme.overview}</span>
            <strong>Nota: {filme.vote_average.toFixed(1)}/10</strong>
            <div className="filme-buttons">
                <button onClick={salvarFilme}>Salvar</button>
                <button><a href={`https://youtube.com/results?search_query=${filme.title} Trailer`} target="blank" rel="external">Trailer</a></button>
            </div>
        </div>
    );
}

export default Filme;