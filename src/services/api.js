import axios from 'axios';

const api = axios.create({
    baseURL: 'https://teteu-filmes-server.onrender.com/',
});

export default api;